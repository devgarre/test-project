﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUpSpawner : MonoBehaviour {

	private int pUpType;
	public List<GameObject> powerUps;
	private GameObject newPowerUp;

	// Use this for initialization
	void Start () {
		if (GameManager.instance.seedMode != 0) {
			Random.InitState (GameManager.instance.worldSeed);
			pUpType = Random.Range (0, powerUps.Count);
		} else {
			pUpType = Random.Range (0, powerUps.Count);
		}
		newPowerUp = Instantiate (powerUps [pUpType]);
		newPowerUp.transform.parent = this.transform;
		newPowerUp.transform.localPosition = new Vector3 (0, 1, 0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}