﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetSettings : MonoBehaviour {

	public Button resetButton;

	// Use this for initialization
	void Start () {
		resetButton.onClick.AddListener (ResetAll);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ResetAll(){
		PlayerPrefs.SetInt ("highScore", 0);
		PlayerPrefs.SetInt ("setSeedMode", 0);
		PlayerPrefs.SetString ("customSeed", "0");
		PlayerPrefs.SetInt ("playerNum", 0);
		PlayerPrefs.SetFloat ("currentWidth", 3);
		PlayerPrefs.SetFloat ("currentHeight", 3);
		PlayerPrefs.SetFloat ("musicVolume", 100);
		PlayerPrefs.SetFloat ("sfxVolume", 100);
	}
}
