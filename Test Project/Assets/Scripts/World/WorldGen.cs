﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen : MonoBehaviour {

	public List<GameObject> gameTiles;
	public float xSize;
	public float zSize;

	public float numOfColumns;
	public float numOfRows;

	public GameObject playerOne;
	public GameObject playerTwo;
	public int PSx;
	public int PSz;

	public GameObject gameplay;
	public GameObject gameplayUI;
	public GameObject gameOver;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.Find ("Tank") == null && GameObject.Find ("Tank 2") == null) {
			gameplay.SetActive (false);
			gameplayUI.SetActive (false);
			gameOver.SetActive (true);
		}
	}

	void OnEnable(){
		numOfRows = PlayerPrefs.GetFloat("currentWidth");
		numOfColumns = PlayerPrefs.GetFloat("currentHeight");
		print (numOfRows);
		print (numOfColumns);
		if (GameManager.instance.seedMode != 0) {
			Random.InitState (GameManager.instance.worldSeed);
			mapGen ();
		} else {
			mapGen ();
		}
	}

	void mapGen (){
		for (float currentRow = 0; currentRow < numOfRows; currentRow++) {
			for (float currentColumn = 0; currentColumn < numOfColumns; currentColumn++) {
				int rand = Random.Range (0, gameTiles.Count);
				GameObject newTile = Instantiate (gameTiles [rand]) as GameObject;
				newTile.name = "Tile (" + currentRow + "," + currentColumn + ")";
				newTile.transform.parent = this.transform;
				float xPos = currentColumn * xSize;
				float zPos = currentRow * zSize;
				newTile.transform.localPosition = new Vector3 (xPos, 0, zPos);
				newTile.GetComponent<DoorDestroy>().xPos = currentRow;
				newTile.GetComponent<DoorDestroy>().zPos = currentColumn;
				newTile.GetComponent<DoorDestroy>().xMax = numOfRows;
				newTile.GetComponent<DoorDestroy>().zMax = numOfColumns;
//				int randRot = Random.Range (0, 3);
//				newTile.transform.Rotate (new Vector3 (0, 90 * randRot, 0));
			}
		}
		if (GameObject.Find("Tank") == null) {
			PSx = Mathf.RoundToInt(Random.Range (0, numOfColumns -1));
			PSz = Mathf.RoundToInt(Random.Range (0, numOfRows -1));
			GameObject newPlayer = Instantiate (playerOne);
			newPlayer.transform.parent = this.transform;
			newPlayer.name = "Tank";
			newPlayer.transform.localPosition = new Vector3 (PSx * xSize, 1, PSz * zSize);
		}
		if (GameObject.Find("Tank 2") == null && PlayerPrefs.GetInt ("playerNum") == 1) {
			PSx = Mathf.RoundToInt(Random.Range (0, numOfColumns -1));
			PSz = Mathf.RoundToInt(Random.Range (0, numOfRows -1));
			GameObject newPlayer = Instantiate (playerTwo);
			newPlayer.transform.parent = this.transform;
			newPlayer.name = "Tank 2";
			newPlayer.transform.localPosition = new Vector3 (PSx * xSize, 1, PSz * zSize);
		}
	}
}
