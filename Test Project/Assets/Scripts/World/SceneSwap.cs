﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneSwap : MonoBehaviour {

	public Button resetButton;

	// Use this for initialization
	void Start () {
		resetButton.onClick.AddListener (runReset);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void runReset(){
		SceneManager.LoadScene ("Reset");
	}
}