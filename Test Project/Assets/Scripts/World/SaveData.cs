﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveData : MonoBehaviour {

	public Button saveButton;
	public Slider widthSlider;
	public Slider heightSlider;
	public InputField seedInput;
	public Dropdown seedMode;
	public Slider musicSlider;
	public Slider sfxSlider;
	public Dropdown playerAmount;

	public AudioSource menuMusic;
	public AudioSource gameMusic;

	// Use this for initialization
	void Start () {
		saveButton.onClick.AddListener (SaveSettings);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SaveSettings(){
		PlayerPrefs.SetInt ("setSeedMode", seedMode.value);
		PlayerPrefs.SetString ("customSeed", seedInput.text);
		PlayerPrefs.SetInt ("playerNum", playerAmount.value);
		PlayerPrefs.SetFloat ("currentWidth", widthSlider.value);
		PlayerPrefs.SetFloat ("currentHeight", heightSlider.value);
		PlayerPrefs.SetFloat ("musicVolume", musicSlider.value);
		PlayerPrefs.SetFloat ("sfxVolume", sfxSlider.value);
		menuMusic.volume = PlayerPrefs.GetFloat ("musicVolume") / 100;
		gameMusic.volume = PlayerPrefs.GetFloat ("musicVolume") / 100;
	}
}
