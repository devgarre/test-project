﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitSettings : MonoBehaviour {

	public Button optionsButton;
	public Slider widthSlider;
	public Slider heightSlider;
	public InputField seedInput;
	public Dropdown seedMode;
	public Slider musicSlider;
	public Slider sfxSlider;
	public Dropdown playerAmount;

	// Use this for initialization
	void Start () {
		optionsButton.onClick.AddListener (InitializeSettings);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void InitializeSettings(){
		widthSlider.value = PlayerPrefs.GetFloat ("currentWidth");
		heightSlider.value = PlayerPrefs.GetFloat ("currentHeight");
		seedInput.text = PlayerPrefs.GetString ("customSeed");
		seedMode.value = PlayerPrefs.GetInt ("setSeedMode");
		musicSlider.value = PlayerPrefs.GetFloat ("musicVolume");
		sfxSlider.value = PlayerPrefs.GetFloat ("sfxVolume");
		playerAmount.value = PlayerPrefs.GetInt ("playerNum");
	}
}
