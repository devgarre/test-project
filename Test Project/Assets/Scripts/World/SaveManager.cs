﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour {



	// Use this for initialization
	void Awake () {
		if (!PlayerPrefs.HasKey ("highScore")) {
			PlayerPrefs.SetInt ("highScore", 0);
		}
		if (!PlayerPrefs.HasKey ("setSeedMode")) {
			PlayerPrefs.SetInt ("setSeedMode", 0);
		}
		if (!PlayerPrefs.HasKey ("customSeed")) {
			PlayerPrefs.SetString ("customSeed", "0");
		}
		if (!PlayerPrefs.HasKey ("playerNum")) {
			PlayerPrefs.SetInt ("playerNum", 0);
		}
		if (!PlayerPrefs.HasKey ("currentWidth")) {
			PlayerPrefs.SetFloat ("currentWidth", 3);
		}
		if (!PlayerPrefs.HasKey ("currentHeight")) {
			PlayerPrefs.SetFloat ("currentHeight", 3);
		}
		if (!PlayerPrefs.HasKey ("musicVolume")) {
			PlayerPrefs.SetFloat ("musicVolume", 100);
		}
		if (!PlayerPrefs.HasKey ("sfxVolume")) {
			PlayerPrefs.SetFloat ("sfxVolume", 100);
		}
		PlayerPrefs.SetInt ("tempScoreOne", 0);
		PlayerPrefs.SetInt ("tempScoreTwo", 0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
