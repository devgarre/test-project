﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public InputController player;
	public AIController enemy;
	public WorldGen Tiles;

    public int seedMode;
	public int worldSeed;

	// Use this for initialization
	void Awake () {
		//instances a game manager if one does not exist
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
		seedMode = PlayerPrefs.GetInt ("setSeedMode");
		if (seedMode == 1) {
			worldSeed = int.Parse (System.DateTime.Now.ToString ("yyyyMMdd"));
		} else if (seedMode == 2) {
			worldSeed = PlayerPrefs.GetInt ("customSeed");
		}
	}

	void Update(){
		seedMode = PlayerPrefs.GetInt ("setSeedMode");
		if (seedMode == 1) {
			worldSeed = int.Parse(System.DateTime.Now.ToString("yyyyMMdd"));
		}
		else if (seedMode == 2) {
			worldSeed = int.Parse(PlayerPrefs.GetString ("customSeed"));
		}
	}
}
