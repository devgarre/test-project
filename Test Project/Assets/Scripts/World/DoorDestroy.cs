﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorDestroy : MonoBehaviour {

	public float xPos;
	public float zPos;
	public float xMax;
	public float zMax;

	// Use this for initialization
	void Start () {
		foreach (Transform child in transform) {
			if (child.gameObject.name == ("Door N") && zPos != 0) {
				Destroy (child.gameObject);
			}
			if (child.gameObject.name == ("Door W") && xPos != 0) {
				Destroy (child.gameObject);
			}
			if (child.gameObject.name == ("Door S") && zPos != zMax - 1) {
				Destroy (child.gameObject);
			}
			if (child.gameObject.name == ("Door E") && xPos != xMax - 1) {
				Destroy (child.gameObject);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
