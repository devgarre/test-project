﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

	public Text endScoreOne;
	public Text endScoreTwo;
	public Text highScore;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable(){
		//sets the score readouts on the game over screen to display the highest scoer achieved and the score for each player
		endScoreOne.text = "P1 Score: " + PlayerPrefs.GetInt("tempScoreOne");
		endScoreTwo.text = "P2 Score: " + PlayerPrefs.GetInt("tempScoreTwo");
		highScore.text =  "High Score: " + PlayerPrefs.GetInt("highScore");
	}
}
