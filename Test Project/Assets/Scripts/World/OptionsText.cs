﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsText : MonoBehaviour {

	public Text resizeText;
	public Slider slider;

	// Use this for initialization
	void Start () {
		slider = GetComponent<Slider> ();
	}
	
	// Update is called once per frame
	void Update () {
		resizeText.text = resizeText.name + ": " + slider.value ;
	}
}
