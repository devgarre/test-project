﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

	[HideInInspector] public TankStats playerStats;
	private float respawnTime = 0.0f;
	public float respawnDelay;
	private bool respawned = true;
	public AudioClip pUpSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (new Vector3 (0, 5, 0));
		if (Time.time >= respawnTime && respawned == false) {
			if(gameObject.tag == "RateOfFire"){
				playerStats.delay += 0.25f;
			}
			if(gameObject.tag == "Speed"){
				playerStats.playerSpeed -= 5;
				playerStats.backSpeed -= 3;
				playerStats.rotateSpeed -= 1;
			}
			transform.localPosition = new Vector3 (0, 1, 0);
			respawned = true;
		}
	}

	void OnCollisionEnter (Collision collision){
		if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Player") {
			playerStats = collision.gameObject.GetComponent<TankStats> ();
			if(gameObject.tag == "RateOfFire"){
				playerStats.delay -= 0.25f;
				AudioSource.PlayClipAtPoint (pUpSound, gameObject.transform.position, PlayerPrefs.GetFloat("sfxVolume") / 100);
				transform.localPosition = new Vector3 (0, -5, 0);
				respawnTime = Time.time + respawnDelay;
			}
			if(gameObject.tag == "Health"){
				playerStats.health += 10;
				if (playerStats.health > playerStats.maxHealth){
					playerStats.health = playerStats.maxHealth;
				}
				AudioSource.PlayClipAtPoint (pUpSound, gameObject.transform.position, PlayerPrefs.GetFloat("sfxVolume") / 100);
				transform.localPosition = new Vector3 (0, -5, 0);
				respawnTime = Time.time + respawnDelay;
			}
			if(gameObject.tag == "Speed"){
				playerStats.playerSpeed += 5;
				playerStats.backSpeed += 3;
				playerStats.rotateSpeed += 1;
				AudioSource.PlayClipAtPoint (pUpSound, gameObject.transform.position, PlayerPrefs.GetFloat("sfxVolume") / 100);
				transform.localPosition = new Vector3 (0, -5, 0);
				respawnTime = Time.time + respawnDelay;
			}
			respawned = false;
		}
	}
}