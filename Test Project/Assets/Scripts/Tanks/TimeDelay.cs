﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeDelay : MonoBehaviour {

	public GameObject bullet;
	public Transform firePoint;
	public InputController controller;
	public AIController aiController;
	public TankStats stats;
	[HideInInspector] public GameObject newBullet;
	private Quaternion randomRotation;
	public AudioClip shootSound;

	// Use this for initialization
	void Start () {
		//gets stats and controller input from other scripts
		controller = GetComponent<InputController> ();
		aiController = GetComponent<AIController> ();
		stats = GetComponent<TankStats> ();
		//initializes the first "shot fired time" as when the game starts
		stats.nextOutputTime = Time.time;
	}

	// Update is called once per frame
	void Update () {

	}
	//if the game time is greater than the last shot time plus delay, allows the entity to fire a bullet and tags it as a bullet of the entity
	public void Shooting(){
		if (Time.time >= stats.nextOutputTime) {
			stats.nextOutputTime = Time.time + stats.delay;
			newBullet = Instantiate (bullet, firePoint.position, firePoint.rotation);
			AudioSource.PlayClipAtPoint (shootSound, firePoint.position, PlayerPrefs.GetFloat("sfxVolume") / 100);
			newBullet.tag = stats.bulletTag;
		}
	}
}
