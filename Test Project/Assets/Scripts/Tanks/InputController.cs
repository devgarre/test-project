﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

	public TankStats pawn;
	public MovementScript move;
	public TimeDelay shooter;

	//declares movement and firing keys in a modifiable form
	public KeyCode leftKey;
	public KeyCode rightKey;
	public KeyCode forwardKey;
	public KeyCode backKey;
	public KeyCode fireKey;
	public KeyCode pauseKey;
	public GameObject pause;
	public GameObject gameplay;
	public GameObject gameplayUI;

	// Use this for initialization
	void Start () {
		//declares object attached to code as player
		GameManager.instance.player = this;
		move = GetComponent<MovementScript> ();
		shooter = GetComponent<TimeDelay> ();
	}

	// Update is called once per frame
	void Update () {
		//if a key is pressed, calls the function from the movement or shooter script
		if (Input.GetKey (forwardKey)) {
			move.forward ();
		}
		if (Input.GetKey (backKey)) {
			move.back ();
		}
		if (Input.GetKey (leftKey)) {
			move.left ();
		}
		if (Input.GetKey (rightKey)) {
			move.right ();
		}
		if (Input.GetKey (fireKey)) {
			shooter.Shooting ();
		}
		if (Input.GetKeyDown (pauseKey)) {
			pause = GameObject.Find ("Pause Parent");
			gameplay = GameObject.Find ("Gameplay");
			gameplayUI = GameObject.Find("Gameplay UI");
			pause.gameObject.transform.GetChild(0).gameObject.SetActive (true);
			gameplayUI.SetActive (false);
			gameplay.SetActive (false);
		}
	}
}
