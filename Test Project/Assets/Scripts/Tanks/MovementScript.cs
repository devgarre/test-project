﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

	//declares variables
	[HideInInspector] public Transform tf;
	private TankStats stats;
	private InputController controller;
	private AIController aiController;

	// Use this for initialization
	void Start () {
		//grabs tank speed and user input
		stats = GetComponent<TankStats> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	//functions are called from other scripts in order to move
	public void left(){
		transform.Rotate (new Vector3 (0, stats.rotateSpeed * -1, 0));
	}

	public void right(){
		transform.Rotate (new Vector3 (0, stats.rotateSpeed, 0));
	}

	public void forward(){
		transform.position += transform.forward * stats.playerSpeed * Time.deltaTime;
	}

	public void back(){
		transform.position -= transform.forward * stats.backSpeed * Time.deltaTime;
	}
}