﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankSpawner : MonoBehaviour {

	public List<GameObject> enemyTanks;	
	public List<GameObject> waypoints;

	GameObject newEnemy;
	int rand;

	// Use this for initialization
	void Start () {
		if (GameManager.instance.seedMode != 0) {
			Random.InitState (GameManager.instance.worldSeed);
			rand = Random.Range (0, enemyTanks.Count);
			newEnemy = Instantiate (enemyTanks [rand], this.transform.position, this.transform.rotation);
			newEnemy.transform.parent = this.transform;
			newEnemy.GetComponent<AIController>().enemyWaypoints = waypoints;
		} else {
			rand = Random.Range (0, enemyTanks.Count);
			newEnemy = Instantiate (enemyTanks [rand], this.transform.position, this.transform.rotation);
			newEnemy.transform.parent = this.transform;
			newEnemy.GetComponent<AIController>().enemyWaypoints = waypoints;
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (newEnemy == null) {
			newEnemy = Instantiate (enemyTanks [rand], this.transform.position, this.transform.rotation);
			newEnemy.GetComponent<AIController>().enemyWaypoints = waypoints;
		}
	}
}
