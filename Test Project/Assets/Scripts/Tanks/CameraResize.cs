﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraResize : MonoBehaviour {

	public Camera playerCam;
	private TankStats stats;
	private int numOfPlayers;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable(){
		stats = GetComponent<TankStats> ();
		numOfPlayers = PlayerPrefs.GetInt ("playerNum");
		if (numOfPlayers == 0) {
			playerCam.rect = new Rect (0.0f, 0.0f, 1.0f, 1.0f);
		} else {
			playerCam.rect = new Rect (0.5f * stats.playerNumber, 0.0f, 0.5f, 1.0f);
		}
	}
}
