﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementScript))]
[RequireComponent(typeof(TimeDelay))]

public class TankStats : MonoBehaviour {

	//declares all tank stats
	public int playerNumber;
	public float playerSpeed;
	public float backSpeed;
	public float rotateSpeed;
	public int score;
	public float delay;
	public int damage;
	public float lives;
	public float health;
	public float maxHealth;
	public float criticalHealth;
	public float healAmount;
	public float fleeDelay;
	public string bulletTag;
	public float patrolDelay;
	[HideInInspector] public float nextOutputTime;

	[HideInInspector] public MovementScript movement;
	[HideInInspector] public TimeDelay shooter;

	// Use this for initialization
	void Start () {
		movement = GetComponent<MovementScript> ();
		shooter = GetComponent<TimeDelay> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
