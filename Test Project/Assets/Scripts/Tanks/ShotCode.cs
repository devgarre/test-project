﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotCode : MonoBehaviour {

	public float speed;
	public float delay;
	public Rigidbody rb;
	private bool justFired;
	[HideInInspector] public TankStats playerStats;
	[HideInInspector] public TankStats enemyStats;
	[HideInInspector] public TankStats shooterStats;
	[HideInInspector] public WorldGen generator;
	private float PSx;
	private float PSz;
	public AudioClip impactSound;
	public AudioClip deathSound;

	// Use this for initialization
	void Start () {
		//assigns necessary variables and objects
		justFired = true;
		rb = GetComponent<Rigidbody> ();
		//destroys bullet after delay
		Destroy (gameObject, delay);
		//adds force to the bullet on spawn
		rb.AddForce(transform.forward * speed);
		//causes the enemy to have bullet spread
		if (gameObject.tag == "Enemy1") {
			rb.AddForce (transform.right * Random.Range(-2.0f, 2.0f));
		}
		generator = GameObject.Find("WorldGen").GetComponent<WorldGen> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerExit(Collider trigger){
		//checks if the bullet has been assigned damage, assigns damage if it hasn't
		if (justFired == true) {
			shooterStats = trigger.gameObject.GetComponent<TankStats> ();
			justFired = false;
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		//sets enemy health on hit
		if (collision.gameObject.tag == "Enemy") {
			enemyStats = collision.gameObject.GetComponent<TankStats> ();
			enemyStats.health -= shooterStats.damage;
			//checks if enemy has health left, destroys them if they dont
			if (enemyStats.health <= 0){
				AudioSource.PlayClipAtPoint (deathSound, gameObject.transform.position, PlayerPrefs.GetFloat("sfxVolume") / 100);
				Destroy (collision.gameObject);
				shooterStats.score += 50;
				if (shooterStats.playerNumber == 0) {
					PlayerPrefs.SetInt ("tempScoreOne", shooterStats.score);
				}
				if (shooterStats.playerNumber == 1) {
					PlayerPrefs.SetInt ("tempScoreTwo", shooterStats.score);
				}
			}
		}
		//sets player health on hit 
		if (collision.gameObject.tag == "Player") {
			playerStats = collision.gameObject.GetComponent<TankStats> ();
			playerStats.health -= shooterStats.damage;
			//checks if player has health left, resets score and health if they dont
			if (playerStats.health <= 0) {
				AudioSource.PlayClipAtPoint (deathSound, gameObject.transform.position, PlayerPrefs.GetFloat("sfxVolume") / 100);
				playerStats.health = playerStats.maxHealth;
				playerStats.lives--;
				if (playerStats.lives <= 0) {
					if (playerStats.score > PlayerPrefs.GetInt ("highScore")) {
						PlayerPrefs.SetInt ("highScore", playerStats.score);
					}
					int truePlayerNum = playerStats.playerNumber + 1;
					Text deathText = GameObject.Find ("Death " + truePlayerNum).GetComponent<Text>();
					if (PlayerPrefs.GetInt ("playerNum") == 0) {
						deathText.transform.position = new Vector3 (Screen.width / 2, Screen.height / 2, 1);
					} else {
						int temp = truePlayerNum + playerStats.playerNumber;
						deathText.transform.position = new Vector3 (Screen.width / 4 * temp, Screen.height / 2, 1);
					}
					Destroy (collision.gameObject);
				} else {
					PSx = Mathf.RoundToInt(Random.Range (0, generator.numOfColumns -1));
					PSz = Mathf.RoundToInt(Random.Range (0, generator.numOfRows -1));
					collision.transform.localPosition = new Vector3 (PSx * generator.xSize, 1, PSz * generator.zSize);
				}
			}
		}
		AudioSource.PlayClipAtPoint (impactSound, gameObject.transform.position, PlayerPrefs.GetFloat("sfxVolume") / 100);
		Destroy (gameObject);
	}
}