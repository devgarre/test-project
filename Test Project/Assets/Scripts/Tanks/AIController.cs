﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

	public MovementScript move;
	public TimeDelay shooter;
	public TankStats stats;

	float fleeTimer;
	public float patrolTimer;

	public enum Actions {Waypoint, Patrol, Chase, Flee, Shoot};
	public Actions currentAction;
	public int currentWaypoint;
	public float nearby;

	public List<GameObject> enemyWaypoints;

	[SerializeField] private float halfView;
	private float dotProd;
	private int wallMask;
	private int enemyMask;
	private int playerMask;
	private int bulletMask;
	public Transform playerPos;
	Vector3 currentPos;
	Vector3 currentVector;
	Vector3 targetPos;
	Vector3 targetVector;
	Vector3 shotPos;

	// Use this for initialization
	//initializes multimodal tank logic
	void Start () {
		GameManager.instance.enemy = this;
		Actions currentAction;
		currentAction = Actions.Waypoint;
		stats = GetComponent<TankStats> ();
		move = GetComponent<MovementScript> ();
		shooter = GetComponent<TimeDelay> ();

		wallMask = 1 << LayerMask.NameToLayer ("Wall");
		enemyMask = 1 << LayerMask.NameToLayer ("Enemy");
		playerMask = 1 << LayerMask.NameToLayer ("Player");
		bulletMask = 1 << LayerMask.NameToLayer ("Bullet");
	}
	
	// Update is called once per frame
	void Update () {
		//sets AI to flee state to regenerate if below a health threshold
		if (stats.health <= stats.criticalHealth){
			currentAction = Actions.Flee;
		}

		//calls functions based on enum logic
		if (currentAction == Actions.Waypoint){
			Waypoint ();
		}
		if (currentAction == Actions.Patrol){
			Patrol ();
		}
		if (currentAction == Actions.Chase){
			Chase ();
		}
		if (currentAction == Actions.Flee){
			Flee ();
		}
		if (currentAction == Actions.Shoot) {
			Shoot (playerPos);
		}
	}

	//sphere vision script
	void OnTriggerStay(Collider detector){
		currentPos = transform.position;
		currentVector = transform.forward;
		targetPos = detector.transform.position;
		targetVector = targetPos - currentPos;

		//generates a limited vision cone in front of the enemy by raycasting to each object within the trigger box and then determining if the angle from the enemy to the object is within the designer set parameters
		float mag = Vector3.SqrMagnitude (currentVector) * Vector3.SqrMagnitude (targetVector);
		if (mag == 0f) {return;}

		dotProd = Vector3.Dot (currentVector, targetVector);
		bool isNegative = dotProd < 0f;
		dotProd = dotProd * dotProd;
		if (isNegative) {dotProd *= -1;}
		float sqrAngle = Mathf.Rad2Deg * Mathf.Acos (dotProd / mag);
		bool isInFront = sqrAngle < halfView;

		//draws a line in the scene view to each object inside of the detection sphere, if the object is in the view cone it colors the line red, otherwise it colors it red
		Debug.DrawLine (currentPos, targetPos, isInFront ? Color.green : Color.red);
//		if (Physics.Linecast (currentPos, targetPos, bulletMask)) {
//			shotPos = targetPos;
//		}
		if (isInFront) {
//			if (!Physics.Linecast (currentPos, targetPos, wallMask)) {
//			}
			//Detects if the player is within the enemy's viewcone. If the player is, it sets the enemy to chase the player's last seen position and begin firing
			if (Physics.Linecast (currentPos, targetPos, playerMask)) {
				playerPos = detector.transform;
				currentAction = Actions.Shoot;
			} else {
				//object avoidance
				if (Physics.Linecast (currentPos, targetPos, wallMask) || Physics.Linecast (currentPos, targetPos, enemyMask)) {
					if (sqrAngle <= 90 && !isNegative) {
						move.right ();
					}
					if (sqrAngle <= 90 && isNegative) {
						move.left ();
					}
					if (sqrAngle > 90 && isNegative) {
						move.right ();
					}
					if (sqrAngle > 90 && !isNegative) {
						move.left ();
					}
				}
			}
		}
	}

	//emun logic that follows predetermined waypoints
	void Waypoint(){
		if (Vector3.Distance(enemyWaypoints[currentWaypoint].transform.position, transform.position) >= nearby){
			MoveTowards (enemyWaypoints [currentWaypoint].transform);
			//if the enemy is within a certain distance of the waypoint, it changes target to next waypoint
			if(Vector3.Distance(enemyWaypoints[currentWaypoint].transform.position, transform.position) <= nearby){
				currentWaypoint += 1;
				//if the enemy would target an out of range waypoint, instead it resets to targeting the original waypoint
				if(currentWaypoint >= enemyWaypoints.Count){
					currentWaypoint = 0;
				}
			}
		}
	}

	//enum logic that patrols by avoiding obstacles until it detects the player
	void Patrol(){
		move.forward ();
		if (Time.time >= patrolTimer){
			currentAction = Actions.Waypoint;
		}
	}

	//enum logic that chases the player's current location
	void Chase(){
		MoveTowards (playerPos);
		if (Vector3.Distance(transform.position, playerPos.position) <= nearby) {
			patrolTimer = Time.time + stats.patrolDelay;
			currentAction = Actions.Patrol;
		}
	}

	//enum logic that runs away from the player
	void Flee(){
		transform.LookAt (shotPos);
		move.back ();
		if (Time.time <= fleeTimer && stats.health < stats.criticalHealth) {
			fleeTimer = Time.time + stats.fleeDelay;
			stats.health += stats.healAmount;
		} else {
			patrolTimer = Time.time + stats.patrolDelay;
			currentAction = Actions.Patrol;
		}

	}

	//enum logic that shoots at the player while the player is visible
	void Shoot (Transform target){
		transform.LookAt(target);
		shooter.Shooting ();
		if (!Physics.Linecast (currentPos, target.position, playerMask)) {
			currentAction = Actions.Chase;
		}
	}

	//moves towards a given point
	void MoveTowards(Transform target){
		transform.LookAt (target.position);
		move.forward ();
	}
}