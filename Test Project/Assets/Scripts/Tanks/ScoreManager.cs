﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public TankStats stats;
	public Text scoreText;
	public Text healthText;
	public Text livesText;
	public Text deathText;
	private float playerNumMod;
	private int truePlayerNum;

	// Use this for initialization
	void Start () {
		deathText = GameObject.Find ("Death " + truePlayerNum).GetComponent<Text>();
		deathText.transform.position = new Vector3 (Screen.width * -1, Screen.height * -1, 1);
	}
	
	// Update is called once per frame
	void Update () {
		if (stats.lives > 0) {
			scoreText.text = "" + stats.score;
			healthText.text = "Health: " + stats.health;
			livesText.text = "Lives: " + stats.lives;
		}
	}

	void OnEnable(){
		stats = GetComponent<TankStats> ();
		truePlayerNum = stats.playerNumber + 1;
		scoreText = GameObject.Find ("Score " + truePlayerNum).GetComponent<Text>();
		healthText = GameObject.Find ("Health " + truePlayerNum).GetComponent<Text>();
		livesText = GameObject.Find ("Lives " + truePlayerNum).GetComponent<Text>();
		int numOfPlayers = PlayerPrefs.GetInt ("playerNum");
		if (numOfPlayers == 0) {
			scoreText.transform.position = new Vector3 (Screen.width - 90.0f, Screen.height - 20.0f, 1);
			healthText.transform.position = new Vector3 (90.0f, Screen.height - 20.0f, 1);
			livesText.transform.position = new Vector3 (90.0f, Screen.height - 40.0f, 1);
		} else {
			playerNumMod = stats.playerNumber * 0.5f;
			scoreText.transform.position = new Vector3 (Screen.width * playerNumMod + Screen.width * 0.5f - 90.0f, Screen.height - 20.0f, 1);
			healthText.transform.position = new Vector3 (Screen.width * playerNumMod + 90.0f, Screen.height - 20.0f, 1);
			livesText.transform.position = new Vector3 (Screen.width * playerNumMod + 90.0f, Screen.height - 40.0f, 1);
		}
	}
}
